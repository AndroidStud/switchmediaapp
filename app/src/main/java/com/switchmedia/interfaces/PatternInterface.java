package com.switchmedia.interfaces;


import com.switchmedia.models.PatternsRootModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PatternInterface {
    @GET(".")
    Call<PatternsRootModel> getImageAndTitle();
}
