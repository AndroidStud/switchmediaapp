package com.switchmedia.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.switchmedia.helper.MainViewHolder;
import com.switchmedia.R;

import java.util.ArrayList;

public class PatternAdapter extends RecyclerView.Adapter<MainViewHolder> {

  private Context context;
  private ArrayList<String> listForImageUrlData;
  private int positionInList = 0;

  public PatternAdapter(Context context) {
    this.context = context;
  }

  @Override
  public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

    View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_list, null);
    MainViewHolder rcv = new MainViewHolder(layoutView);
    return rcv;
  }

  @Override
  public void onBindViewHolder(MainViewHolder holder, int position) {
    //To make it infinite loop, with 1st view visible again as soon as last reaches.
    positionInList = positionInList + (position % 4);

    if(positionInList+7 >= listForImageUrlData.size())
    {
      // To avoid arrayIndexOutOfBound exception
      positionInList = 0;
    }
    setImage(positionInList,holder.imgPhotoBig);
    setImage(positionInList+1,holder.imgPhotoSmall1);
    setImage(positionInList+2,holder.imgPhotoSmall2);
    setImage(positionInList+3,holder.imgPhotoSmall3);
    setImage(positionInList+4,holder.imgPhotoSmall4);
    setImage(positionInList+5,holder.imgPhotoSmall5);
    setImage(positionInList+6,holder.imgPhotoSmall6);

    setText(positionInList,holder.txtTitleBig);
    setText(positionInList+1,holder.txtTitleSmall1);
    setText(positionInList+2,holder.txtTitleSmall2);
    setText(positionInList+3,holder.txtTitleSmall3);
    setText(positionInList+4,holder.txtTitleSmall4);
    setText(positionInList+5,holder.txtTitleSmall5);
    setText(positionInList+6,holder.txtTitleSmall6);

     positionInList = positionInList + 7;
  }

  private void setText(int positionInList, TextView txtForTitle) {
    txtForTitle.setText("Title "+positionInList+"");
  }

  @Override
  public int getItemCount() {
    return Integer.MAX_VALUE;
  }

    public void setArrayListForUrlData(ArrayList<String> mParam1) {
      this.listForImageUrlData = mParam1;
    }

    void setImage(int position, ImageView imgHolder)
    {
      Glide.with(context)
              .load(listForImageUrlData.get(position))
              .placeholder(android.R.drawable.ic_dialog_alert)
              .into(imgHolder);
    }
}