package com.switchmedia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.switchmedia.utils.Constants;
import com.switchmedia.interfaces.PatternInterface;
import com.switchmedia.models.PatternsRootModel;
import com.switchmedia.R;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Display splash screen till the time image url path are not downloaded with the help of retrofit.
 */
public class SplashScreen extends AppCompatActivity {

    final ArrayList<String> imageUrls = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getDataFromServer();
    }

    private void getDataFromServer() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.url)
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();

        PatternInterface apiService = retrofit.create(PatternInterface.class);
        imageUrls.clear();
        //Displaying 4 view, with 1 bigger image and 3 sets of smaller images stacked ove each other.
        for (int i = 0; i < 28; i++) {
            Call<PatternsRootModel> shiftModel = apiService.getImageAndTitle();

            final int finalI = i;
            shiftModel.enqueue(new Callback<PatternsRootModel>() {
                @Override
                public void onResponse(Call<PatternsRootModel> call, Response<PatternsRootModel> response) {
                    Log.d("imageUrl******", response.body().pattern.get(0).imageUrl);
                    imageUrls.add(response.body().pattern.get(0).imageUrl);
                    if (finalI == 27) {
                        callMainActivity();
                    }
                }

                @Override
                public void onFailure(Call<PatternsRootModel> call, Throwable t) {
                }
            });
        }
    }

    private void callMainActivity() {
        Intent i = new Intent(SplashScreen.this, MainActivity.class);
        i.putStringArrayListExtra("imageUrlsArrayList", imageUrls);
        startActivity(i);
        finish();
    }
}
