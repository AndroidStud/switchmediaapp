package com.switchmedia.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.switchmedia.adapters.PatternAdapter;
import com.switchmedia.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    private View parentView;
    private RecyclerView recyclerViewParliament;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private static final String ARG_PARAM1 = "param1";
    private ArrayList<String> mParam1;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(ArrayList<String> param1) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getStringArrayList(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initialiseRecyclerView();
    }

    private void initialiseRecyclerView() {

        recyclerViewParliament = (RecyclerView) getView().findViewById(R.id.recyclerView);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, 0);

        recyclerViewParliament.setLayoutManager(staggeredGridLayoutManager);
        final PatternAdapter rcAdapter = new PatternAdapter(getActivity());
        rcAdapter.setArrayListForUrlData(mParam1);
        recyclerViewParliament.setAdapter(rcAdapter);
    }
}
