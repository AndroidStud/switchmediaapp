package com.switchmedia.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.switchmedia.R;

/**
 * A simple {@link Fragment} subclass.
 * {@link LiveTvFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LiveTvFragment extends Fragment {
    public LiveTvFragment() {
        // Required empty public constructor
    }

    /**
     * A new instance of this fragment using the provided parameters.
     * @param text Parameter 1.
     * @return A new instance of fragment HomeFragment.
     */
    public static LiveTvFragment newInstance(String text) {
        LiveTvFragment firstFragment = new LiveTvFragment();
        Bundle bundle = new Bundle();
        bundle.putString("msg", text);
        firstFragment.setArguments(bundle);
        return firstFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dummy_ui, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView txtFragmentTitle = (TextView) getView().findViewById(R.id.txtFragmentTitle);
        txtFragmentTitle.setText("Live TV Fragment");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
