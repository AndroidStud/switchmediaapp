package com.switchmedia.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Upasana on 4/11/2017.
 */
@Root(strict = false)
public class Pattern {
    @Element(name = "imageUrl")
    public String imageUrl;
}
