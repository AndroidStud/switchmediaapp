package com.switchmedia.models;

/**
 * Created by Upasana on 3/21/2017.
 */

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(strict = false)
public class PatternsRootModel {
    @Attribute
    int numResults;
    @Attribute
    int totalResults;

    @ElementList(inline = true)
    public List<Pattern> pattern;
}

