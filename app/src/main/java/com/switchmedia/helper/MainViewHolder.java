package com.switchmedia.helper;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.switchmedia.R;

/**
 * Bind the widgets and handle click listeners.
 */
public class MainViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imgPhotoBig;
    public ImageView imgPhotoSmall1;
    public ImageView imgPhotoSmall2;
    public ImageView imgPhotoSmall3;
    public ImageView imgPhotoSmall4;
    public ImageView imgPhotoSmall5;
    public ImageView imgPhotoSmall6;

    public TextView txtTitleBig;
    public TextView txtTitleSmall1;
    public TextView txtTitleSmall2;
    public TextView txtTitleSmall3;
    public TextView txtTitleSmall4;
    public TextView txtTitleSmall5;
    public TextView txtTitleSmall6;

    public MainViewHolder(View itemView) {
        super(itemView);

        imgPhotoBig = (ImageView) itemView.findViewById(R.id.imgPhotoBig);
        imgPhotoSmall1 = (ImageView) itemView.findViewById(R.id.imgPhotoSmall1);
        imgPhotoSmall2 = (ImageView) itemView.findViewById(R.id.imgPhotoSmall2);
        imgPhotoSmall3 = (ImageView) itemView.findViewById(R.id.imgPhotoSmall3);
        imgPhotoSmall4 = (ImageView) itemView.findViewById(R.id.imgPhotoSmall4);
        imgPhotoSmall5 = (ImageView) itemView.findViewById(R.id.imgPhotoSmall5);
        imgPhotoSmall6 = (ImageView) itemView.findViewById(R.id.imgPhotoSmall6);

        txtTitleBig = (TextView) itemView.findViewById(R.id.txtTitleBig);
        txtTitleSmall1 = (TextView) itemView.findViewById(R.id.txtTitleSmall1);
        txtTitleSmall2 = (TextView) itemView.findViewById(R.id.txtTitleSmall2);
        txtTitleSmall3 = (TextView) itemView.findViewById(R.id.txtTitleSmall3);
        txtTitleSmall4 = (TextView) itemView.findViewById(R.id.txtTitleSmall4);
        txtTitleSmall5 = (TextView) itemView.findViewById(R.id.txtTitleSmall5);
        txtTitleSmall6 = (TextView) itemView.findViewById(R.id.txtTitleSmall6);

        imgPhotoBig.setOnClickListener(this);
        imgPhotoSmall1.setOnClickListener(this);
        imgPhotoSmall2.setOnClickListener(this);
        imgPhotoSmall3.setOnClickListener(this);
        imgPhotoSmall4.setOnClickListener(this);
        imgPhotoSmall5.setOnClickListener(this);
        imgPhotoSmall6.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgPhotoBig:
                showMyDialog(imgPhotoBig.getDrawable(), txtTitleBig.getText().toString(),view.getContext());
              break;

            case R.id.imgPhotoSmall1:
                showMyDialog(imgPhotoSmall1.getDrawable(), txtTitleSmall1.getText().toString(),view.getContext());
                break;

            case R.id.imgPhotoSmall2:
                showMyDialog(imgPhotoSmall2.getDrawable(), txtTitleSmall2.getText().toString(),view.getContext());
                break;

            case R.id.imgPhotoSmall3:
                showMyDialog(imgPhotoSmall3.getDrawable(), txtTitleSmall3.getText().toString(),view.getContext());
                break;

            case R.id.imgPhotoSmall4:
                showMyDialog(imgPhotoSmall4.getDrawable(), txtTitleSmall4.getText().toString(),view.getContext());
                break;

            case R.id.imgPhotoSmall5:
                showMyDialog(imgPhotoSmall5.getDrawable(), txtTitleSmall5.getText().toString(),view.getContext());
                break;

            case R.id.imgPhotoSmall6:
                showMyDialog(imgPhotoSmall6.getDrawable(), txtTitleSmall6.getText().toString(),view.getContext());
                break;
        }
    }

    private void showMyDialog(Drawable drawable, String title, Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.overlay_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);

        TextView textView = (TextView) dialog.findViewById(R.id.txtPopUpTitleBig);
        ImageView imgPopUpPhoto = (ImageView) dialog.findViewById(R.id.imgPopupPhotoBig);

        textView.setText(title);
        imgPopUpPhoto.setImageDrawable(drawable);
        /**
         * if you want the dialog to be specific size, do the following
         * this will cover 85% of the screen (85% width and 85% height)
         */
//        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
//        int dialogWidth = (int)(displayMetrics.widthPixels * 0.85);
//        int dialogHeight = (int)(displayMetrics.heightPixels * 0.85);
//        dialog.getWindow().setLayout(dialogWidth, dialogHeight);

        dialog.show();
    }


}